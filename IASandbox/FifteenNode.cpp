#include "FifteenNode.h"

FifteenNode::FifteenNode() : hCost(-1)
{
}

FifteenNode::FifteenNode(const FifteenConfiguration * i_config) : FifteenNode()
{
	for (int i = 0; i < MyConfiguration.sizeSquare; ++i)
	{
		MyConfiguration.config[i] = i_config->config[i];
	}
}

std::shared_ptr<FifteenNode> FifteenNode::GetParent() const
{
	return Parent;
}

const FifteenConfiguration * FifteenNode::GetConfiguration() const
{
	return &MyConfiguration;
}

int FifteenNode::GetHCost()
{
	if (hCost < 0)
	{
		auto c = MyConfiguration.config;
		hCost = 0;
		for (int i = 0; i < MyConfiguration.sizeSquare ; ++i)
		{
			if (c[i] != 0) 
			{
				int DesiredX = c[i] / MyConfiguration.size;
				int DesiredY = c[i] % MyConfiguration.size; 

				int CurrentX = i / MyConfiguration.size;
				int CurrentY = i % MyConfiguration.size;

				int hX = abs(CurrentX - DesiredX);
				int hY = abs(CurrentY - DesiredY);

				hCost += hX + hY;
			}
		}
	}

	return hCost;
}

int FifteenNode::GetGCost() const
{
	return gCost;
}

int FifteenNode::GetFCost() const
{
	return fCost;
}

void FifteenNode::GetAdjacency(std::list<std::shared_ptr<FifteenNode>>& outadj)
{
	if (Adjacency.empty())
	{
		auto c = MyConfiguration.config;
		int index = -1;
		for (int i = 0; i < MyConfiguration.sizeSquare; ++i)
		{
			if (c[i] == 0)
			{
				index = i;
				break;
			}
		}

		int up = index - MyConfiguration.size;
		int down = index + MyConfiguration.size;
		int left = index - 1;
		int right = index + 1;

		if (up >= 0)
		{
			std::shared_ptr<FifteenNode> temp(new FifteenNode(&MyConfiguration));

			temp->MyConfiguration.config[index] ^= temp->MyConfiguration.config[up];
			temp->MyConfiguration.config[up] ^= temp->MyConfiguration.config[index];
			Adjacency.push_back(temp);
		}

		if (down < MyConfiguration.sizeSquare)
		{
			std::shared_ptr<FifteenNode> temp(new FifteenNode(&MyConfiguration));

			temp->MyConfiguration.config[index] ^= temp->MyConfiguration.config[down];
			temp->MyConfiguration.config[down] ^= temp->MyConfiguration.config[index];
			Adjacency.push_back(temp);
		}

		if (left >= 0 && left/MyConfiguration.size == index/ MyConfiguration.size)
		{
			std::shared_ptr<FifteenNode> temp(new FifteenNode(&MyConfiguration));

			temp->MyConfiguration.config[index] ^= temp->MyConfiguration.config[left];
			temp->MyConfiguration.config[left] ^= temp->MyConfiguration.config[index];
			Adjacency.push_back(temp);
		}

		if (right < MyConfiguration.sizeSquare && right / MyConfiguration.size == index / MyConfiguration.size)
		{
			std::shared_ptr<FifteenNode> temp(new FifteenNode(&MyConfiguration));
			temp->MyConfiguration.config[index] ^= temp->MyConfiguration.config[right];
			temp->MyConfiguration.config[right] ^= temp->MyConfiguration.config[index];
			Adjacency.push_back(temp);
		}

	}
	outadj = Adjacency;
}

void FifteenNode::ChangeParent(std::shared_ptr<FifteenNode> i_NewParent, int i_Cost)
{
	Parent = i_NewParent;
	gCost = i_Cost;
	fCost = gCost + GetHCost();
}

bool FifteenNode::operator==(const FifteenNode & other) const
{
	auto c = other.GetConfiguration();

	for (int i = 0; i < c->sizeSquare; ++i)
	{
		if(c->config[i] != MyConfiguration.config[i])
		{
			return false;
		}
	}

	return true;
}
