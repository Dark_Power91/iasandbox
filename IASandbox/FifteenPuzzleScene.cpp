#include "FifteenPuzzleScene.h"
#include <string>
#include <chrono>
#include <thread>


FifteenPuzzleScene::FifteenPuzzleScene()
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			sf::Text tText;
			tText.setString("");
			tText.setPosition(sf::Vector2f(static_cast<float>(25 * j), static_cast<float>(25 * i + 20)));
			tText.setCharacterSize(20);
			tText.setFont(*(FontManager::Istance()->GetFont(Font_Consola)));
			texts.push_back(tText);
		}
	}
}

FifteenPuzzleScene::~FifteenPuzzleScene()
{
	
}

void FifteenPuzzleScene::OnIdle()
{
	static bool Runing = false;
	if(!Runing)
	{
		alg.Run();
		Runing = true;
	}
}

void FifteenPuzzleScene::OnDraw(sf::RenderWindow& renderWindow)
{
	sf::Text Result;
	Result.setPosition(20,0);
	Result.setCharacterSize(20);
	Result.setOutlineThickness(2);

	Result.setFillColor(sf::Color::Blue); 
	Result.setOutlineColor(sf::Color::Red);

	Result.setFont(*(FontManager::Istance()->GetFont(Font_Consola)));
	if (alg.isEnded())
	{
		Result.setString("Result");
		auto res = alg.GetResultStep();
		if (res != nullptr)
		{
			auto a = res->config;
			for (int i = 0; i < 16; ++i)
			{
				if (a[i] == 0)
				{
					texts.at(i).setString("");
				}
				else
				{
					texts.at(i).setString(std::to_string(a[i]));
				}
			}

		}
		else
		{
			for (auto& text : texts)
			{
				text.setFillColor(sf::Color::Green);
			}
		}

	}
	else
	{
		Result.setString("Wait");
	}

	renderWindow.draw(Result);
	for (auto& text : texts)
	{
		renderWindow.draw(text);
	}
	if (alg.isEnded()) 
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(250));
	}
}