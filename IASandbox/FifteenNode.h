#pragma once

#include <iostream>
#include <list>
#include <memory>
struct  FifteenConfiguration
{
	static const int size = 4;
	static const int sizeSquare = 16;
	int config[size*size] = { 1,2,0,3,
							  4,6,7,11,
							  5,9,14,15,
							  10,8,12,13 }; //{ 2,3,4,12,6,15,0,8,1,7,10,11,5,9,13,14 };

	void PrintConfiguration() const 
	{
		for (int i = 0; i < size; ++i)
		{
			for (int j = 0; j < size; ++j)
			{
				std::cout << config[(i*size) + j]<<"\t";
			}
			std::cout << std::endl << std::endl;
		}
	}
};


class FifteenNode
{
	std::shared_ptr<FifteenNode> Parent;

	std::list<std::shared_ptr<FifteenNode>> Adjacency;
	FifteenConfiguration MyConfiguration;


	int gCost;
	int hCost;
	int fCost;

public:

	FifteenNode();
	FifteenNode(const FifteenConfiguration* i_config);

	std::shared_ptr<FifteenNode> GetParent() const;
	const FifteenConfiguration* GetConfiguration() const;

	int GetHCost();
	int GetGCost() const;
	int GetFCost() const;

	void GetAdjacency(std::list<std::shared_ptr<FifteenNode>> &outadj);

	void ChangeParent(std::shared_ptr<FifteenNode> i_NewParent, int i_Cost);

	bool operator==(const FifteenNode& other) const;
};

