#pragma once
#include <vector>
#include <deque>
#include <memory>
#include "FifteenNode.h"

class FifteenAStar
{
public:
	void Run();
	const FifteenConfiguration * GetStartConfig();
	const FifteenConfiguration * GetResultStep();
	const FifteenConfiguration * GetFinalRestultStep();
	bool isEnded() const;

private:
	FifteenNode Start;
	bool Ended = false;

	const int Goal[16] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 };

	void Clean();
	
	void Search();

	bool GoalTest(std::shared_ptr<FifteenNode> pNode);
	bool isInClosed(std::shared_ptr<FifteenNode> pNode);
	bool isInOpen(std::shared_ptr<FifteenNode> pNode, int& index);

	std::shared_ptr<FifteenNode> VisitNode();
	void AddNodeToOpenList(std::shared_ptr<FifteenNode> pParent, std::shared_ptr<FifteenNode> pNode);


	void PrintPath(std::shared_ptr<FifteenNode> pNode);

	std::deque<std::shared_ptr<FifteenNode>> qOpenList;
	std::vector<std::shared_ptr<FifteenNode>> ClosedList;
	std::list<const FifteenConfiguration*> result;

public:
	inline const FifteenNode* GetStart() const { return &Start; };


};

