#include "Node.h"

//CTor
Node::Node(int i_X, int i_Y)
{
	MyConfig.x = i_X;
	MyConfig.y = i_Y;

	gCost = hCost = fCost = 0;

	State = NodeState::Unknown;
}

//Getter
Node* Node::GetParent() const
{
	return Parent;
}

int Node::GetGCost() const
{
	return gCost;
}

int Node::GetHCost() const
{
	return hCost;
}

int Node::GetFCost() const
{
	return fCost;
}

NodeState Node::GetState() const
{
	return State;
}

const NodeConfiguration * Node::GetConfiguration() const
{
	return &MyConfig;
}

void Node::GetAdjacency(std::list<Node*>& outAdj)
{
	outAdj = Adjacency;
}

//Setter
void Node::SetHCost(int i_hCost)
{
	hCost = i_hCost;
}

void Node::SetState(NodeState i_State)
{
	State = i_State;
}

void Node::AddAdjacency(Node * i_adj)
{
	Adjacency.push_back(i_adj);
}

void Node::ChangeParent(Node * i_NewParent, int i_Cost)
{
	Parent = i_NewParent;
	gCost = i_Cost;

	fCost = hCost + gCost;
}
