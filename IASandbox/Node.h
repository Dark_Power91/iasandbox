#ifndef __AStar__Node__
#define __AStar__Node__

#include <iostream>
#include <list>
#include "NodeState.h"

struct NodeConfiguration
{
	int x;
	int y;

	void PrintConfiguration() const
	{
		std::cout << "X: " << x;
		std::cout << " Y: " << y << std::endl;
	}
};

class Node
{
	Node* Parent;

	std::list<Node*> Adjacency;
	NodeConfiguration MyConfig;
	NodeState State;

	int gCost;
	int hCost;
	int fCost;

public: 
	Node(int i_X, int i_Y);

	Node* GetParent() const;
	const NodeConfiguration* GetConfiguration()const;

	int GetHCost() const;
	int GetGCost() const;
	int GetFCost() const; 

	NodeState GetState() const;
	void GetAdjacency(std::list<Node*> & outAdj);

	void SetHCost(int i_hCost);
	void SetState(NodeState i_State);

	void AddAdjacency(Node* i_adj);

	void ChangeParent(Node* i_NewParent, int i_Cost);
};

#endif /* defined(__AStar__Node__) */
