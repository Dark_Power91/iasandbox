#include <cmath>
#include <list>

#include "AStar.h"

void AStar::Run()
{
	CreateGraph();
	
	CreateGraphAdjs();
	
	ComputeGraphHeuristics();

	AddNodeToOpenList(nullptr, tRoot[iStartNode]);
}

void AStar::DoStep()
{
	Search();
}

void AStar::CreateGraph()
{
	for (int xCoord = 0; xCoord < xMax;++xCoord)
	{
		for (int yCoord = 0; yCoord < yMax; ++yCoord)
		{
			tRoot[((yMax)* xCoord) + yCoord] = new Node(xCoord, yCoord);
		}
	}
}

void AStar::CreateGraphAdjs()
{
	for (int xCoord = 0; xCoord < xMax;++xCoord)
	{
		for (int yCoord = 0; yCoord<yMax;++yCoord)
		{
			CreateNodeAdj(xCoord, yCoord);
		}
	}
}

void AStar::CreateNodeAdj(const int iRow, const int iCol)
{
	const int iNode((iRow * yMax) + iCol);

	Node* cNode = tRoot[iNode];

	int up = iNode - yMax;
	if (up >= 0)
	{
		cNode->AddAdjacency(tRoot[up]);
	}

	int down = iNode + yMax;
	if (down < yMax*xMax)
	{
		cNode->AddAdjacency(tRoot[down]);
	}

	int left = iNode - 1;
	if (left >= 0 && left/yMax == iRow)
	{
		cNode->AddAdjacency(tRoot[left]);
	}

	int right = iNode + 1;
	if (right < yMax*xMax && right / yMax == iRow)
	{
		cNode->AddAdjacency(tRoot[right]);
	}

}

void AStar::ComputeGraphHeuristics()
{
	for (int xCoord = 0; xCoord < xMax;++xCoord)
	{
		for (int yCoord = 0; yCoord<yMax;++yCoord)
		{
			ComputeNodeHeuristic(tRoot[(xCoord * yMax) + yCoord]);
		}
	}
}

void AStar::ComputeNodeHeuristic(Node* pNode)
{
	const NodeConfiguration* GoalConfig = tRoot[iEndNode]->GetConfiguration();
	const NodeConfiguration* pNodeConfig = pNode->GetConfiguration();

	int hX = abs(pNodeConfig->x - GoalConfig->x);
	int hY = abs(pNodeConfig->y - GoalConfig->y);

	pNode->SetHCost(hX + hY);
}

void AStar::Clean()
{
	for (int xCoord = 0; xCoord < xMax; ++xCoord)
	{
		for (int yCoord = 0; yCoord < yMax; ++yCoord)
		{
			delete tRoot[((yMax)* xCoord) + yCoord];
			tRoot[((yMax)* xCoord) + yCoord] = nullptr;
		}
	}
	qOpenList.clear();
	Ended = false;


}

void AStar::Search()
{
	if (!qOpenList.empty() && !Ended)
	{
		Node* currentNode = VisitNode();
		if (currentNode == tRoot[iEndNode])
		{
			currentNode->SetState(NodeState::Solution);
			PrintPath(currentNode);
			Ended = true;
		}
	}
}

Node* AStar::VisitNode()
{
	//Pop the head of the queue
	Node* head = qOpenList.front();
	qOpenList.pop_front();

	//Get the adjacency
	std::list<Node*> adj;
	head->GetAdjacency(adj);

	//add adjacent to openList
	for (auto node : adj)
	{
		AddNodeToOpenList(head, node);
	}

	head->SetState(NodeState::Closed);
	return head;
}

void AStar::AddNodeToOpenList(Node* pParent, Node* pNode)
{
	if (pNode == nullptr) 
		return;

	int gCost = pParent == nullptr ? 0 : pParent->GetGCost() + 1; //suppongo costo di passo 1

	switch (pNode->GetState())
	{
	case NodeState::Unknown:
		pNode->ChangeParent(pParent, gCost);
		pNode->SetState(NodeState::Open);

		if (qOpenList.size() > 0)
		{
			bool broke = false;
			for (std::list<Node*>::iterator NodeIterator = qOpenList.begin(); NodeIterator != qOpenList.end(); ++NodeIterator)
			{
				if ((*NodeIterator)->GetFCost() >= pNode->GetFCost())
				{
					qOpenList.insert(NodeIterator, pNode);
					broke = true;
					break;
				}
			}
			if (!broke)
			{
				qOpenList.push_back(pNode);
			}
		}
		else
		{
			qOpenList.push_front(pNode);
		}
		break;
	case NodeState::Open:
		if (pNode->GetGCost() > gCost)
		{
			pNode->ChangeParent(pParent, gCost);

			qOpenList.remove(pNode);

			for (std::list<Node*>::iterator NodeIterator = qOpenList.begin(); NodeIterator != qOpenList.end(); ++NodeIterator)
			{
				if ((*NodeIterator)->GetFCost() > pNode->GetFCost())
				{
					qOpenList.insert(NodeIterator, pNode);
					break;
				}
			}
		}
		break;
	case NodeState::Closed:

		//if (pNode->GetGCost() > gCost)
		//{
		//	pNode->ChangeParent(pParent, gCost);
		//	pNode->SetState(NodeState::Open);

		//	for (std::list<Node*>::iterator NodeIterator = qOpenList.begin(); NodeIterator != qOpenList.end(); ++NodeIterator)
		//	{
		//		if ((*NodeIterator)->GetFCost() > pNode->GetFCost())
		//		{
		//			qOpenList.insert(NodeIterator, pNode);
		//			break;
		//		}
		//	}
		//}
		break;
	default:
		std::cerr << "Unexpected Behavior";
		break;
	}
}

void AStar::PrintPath(Node* pNode)
{
	if (pNode->GetParent() != nullptr)
		PrintPath(pNode->GetParent());

	const NodeConfiguration* tConfig = pNode->GetConfiguration();
	tConfig->PrintConfiguration();
}



