#pragma once

#include "IScene.h"
#include "FontManager.h"
#include <assert.h>
#include <vector>
#include <list>
#include <time.h>
#include <sstream>
#include "AStar.h"

class AStarScene : public IScene
{

private :
	const sf::Color UnknownColor = sf::Color::White;
	const sf::Color OpenColor = sf::Color::Blue;
	const sf::Color ClosedColor = sf::Color::Red;
	const sf::Color SolutionColor = sf::Color::Yellow;

	AStar alg;
	int xSize;
	int ySize;
	std::vector<sf::CircleShape> table;

public:
	
	AStarScene();
	
	virtual void OnIdle(void);
	virtual void OnDraw(sf::RenderWindow&);
	
	virtual ~AStarScene(void);

};


