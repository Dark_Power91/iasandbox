#ifndef __AStar__AStar__
#define __AStar__AStar__

#include <iostream>
#include <list>
#include "Node.h"

class AStar
{
public:
	void Run();
	void DoStep();
	void Clean();
private:
	const static int xMax = 10;
	const static int yMax = 10;
	const static int iStartNode = 80;
	const static int iEndNode = 3;
	bool Ended = false;
	
	void CreateGraph();
	void CreateGraphAdjs();
	void CreateNodeAdj(const int iRow, const int iCol);
	
	void ComputeGraphHeuristics();
	void ComputeNodeHeuristic(Node* pNode);
	
	
	void Search();
	Node* VisitNode();
	void AddNodeToOpenList(Node* pParent, Node* pNode);
	
	void PrintPath(Node* pNode);

	Node* tRoot[xMax * yMax];
	
	std::list<Node*> qOpenList;

public:
	inline Node** GetConfiguration() { return &(tRoot[0]); };
	inline int GetXSize() const { return xMax; };
	inline int GetYSize() const { return yMax; };
	inline bool IsEnded() const { return Ended; };

};

#endif /* defined(__AStar__AStar__) */