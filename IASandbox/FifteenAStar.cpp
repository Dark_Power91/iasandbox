#include "FifteenAStar.h"
#include "stdlib.h"

void FifteenAStar::Run()
{
	Search();
}

const FifteenConfiguration * FifteenAStar::GetStartConfig()
{
	return Start.GetConfiguration();
}

const FifteenConfiguration * FifteenAStar::GetResultStep()
{
	if (!result.empty())
	{
		const FifteenConfiguration* t = result.front();
		result.pop_front();
		return t;
	}
	else
	{
		return nullptr;
	}
}

const FifteenConfiguration * FifteenAStar::GetFinalRestultStep()
{
	if (isEnded())
	{
		return result.back();
	}
	return nullptr;
}

bool FifteenAStar::isEnded() const
{
	return Ended;
}

void FifteenAStar::Clean()
{
	//TODO Destroy well all Nodes!
}

void FifteenAStar::Search()
{
	AddNodeToOpenList(nullptr, std::shared_ptr<FifteenNode> (&Start));

	while (!qOpenList.empty() && !Ended)
	{
		std::shared_ptr<FifteenNode> currentNode(VisitNode());
		if (GoalTest(currentNode))
		{
			system("cls");
			PrintPath(currentNode);
			Ended = true;
		}
	}
}

bool FifteenAStar::GoalTest(std::shared_ptr<FifteenNode> pNode)
{
	auto config = pNode->GetConfiguration();

	for (int i = 0; i < 16; ++i)
	{
		if(config->config[i] != Goal[i])
		{
			return false;
		}
	}
	return true;
}

bool FifteenAStar::isInClosed(std::shared_ptr<FifteenNode> pNode)
{
	for (auto& node: ClosedList)
	{
		if (pNode->GetHCost() == node->GetHCost())
		{
			if (pNode == node)
			{
				return true;
			}
		}
	}
	return false;
}

bool FifteenAStar::isInOpen(std::shared_ptr<FifteenNode> pNode, int & index)
{
	for (size_t i = 0; i < qOpenList.size(); ++i)
	{
		if (pNode->GetHCost() == qOpenList.at(i)->GetHCost())
		{
			if (pNode == qOpenList.at(i))
			{
				index = i;
				return true;
			}
		}
	}
	return false;
}

std::shared_ptr<FifteenNode> FifteenAStar::VisitNode()
{
	std::shared_ptr<FifteenNode> head = qOpenList.front();
	qOpenList.pop_front();
	ClosedList.push_back(head);

	std::list<std::shared_ptr<FifteenNode>> adj;
	head->GetAdjacency(adj);

	system("cls");
	head->GetConfiguration()->PrintConfiguration();

	for (auto node : adj)
	{
		AddNodeToOpenList(head, node);
	}
	return head;
}

void FifteenAStar::AddNodeToOpenList(std::shared_ptr<FifteenNode> pParent, std::shared_ptr<FifteenNode> pNode)
{
	if (pNode == nullptr)
		return;

	int gCost = pParent == nullptr ? 0 : pParent->GetGCost() + 1;
	pNode->ChangeParent(pParent, gCost);

	if(!isInClosed(pNode))
	{
		int index;
		bool isOpen = isInOpen(pNode, index);
		bool toAdd = false;
		if (isOpen)
		{
			if (qOpenList.at(index)->GetGCost() > gCost)
			{
				qOpenList.erase(qOpenList.begin() + index);
				toAdd = true;
			}
		}
		if (!isOpen || (isOpen&&toAdd)){
			if (qOpenList.size() > 0)
			{
				bool broke = false;
				for (std::deque<std::shared_ptr<FifteenNode>>::iterator NodeIterator = qOpenList.begin(); NodeIterator != qOpenList.end(); ++NodeIterator)
				{
					if ((*NodeIterator)->GetFCost() >= pNode->GetFCost())
					{
						qOpenList.insert(NodeIterator, pNode);
						broke = true;
						break;
					}
				}
				if (!broke)
				{
					qOpenList.push_back(pNode);
				}
			}
			else
			{
				qOpenList.push_front(pNode);
			}
		}
	}
}

void FifteenAStar::PrintPath(std::shared_ptr<FifteenNode> pNode)
{
	if (pNode->GetParent() != nullptr)
		PrintPath(pNode->GetParent());

	const FifteenConfiguration* tConfig = pNode->GetConfiguration();
	tConfig->PrintConfiguration();
	std::cout << std::endl << std::endl;

	result.push_back(tConfig);

	Clean();
}
