#include "AStarScene.h"

AStarScene::AStarScene()
{
	xSize = alg.GetXSize();
	ySize = alg.GetYSize();

	for (int i = 0; i < xSize; i++)
	{
		for (int j = 0; j < ySize; j++)
		{
			sf::CircleShape shape(20);
			shape.setPosition(sf::Vector2f(40 * j, 40 * i));
			shape.setFillColor(sf::Color(100 * i, 250*j, 50 *(i+j)));
			table.push_back(shape);
		}
	}
	alg.Run();

}

AStarScene::~AStarScene(){}

void AStarScene::OnIdle()
{
	//Chiamo la search

	alg.DoStep();

	//Chiamo il getter

	Node**  ProblemState = alg.GetConfiguration();

	// setto i colori
	if (ProblemState[0]!= nullptr) {
		for (int i = 0; i < xSize*ySize; ++i)
		{
			switch (ProblemState[i]->GetState())
			{
			case NodeState::Open:
				table.at(i).setFillColor(OpenColor);
				break;

			case NodeState::Closed:
				table.at(i).setFillColor(ClosedColor);
				break;

			case NodeState::Solution:
				table.at(i).setFillColor(SolutionColor);
				break;

			case NodeState::Unknown:
				table.at(i).setFillColor(UnknownColor);
				break;

			default:
				table.at(i).setFillColor(sf::Color::Black);
				break;
			}
		}
		if (alg.IsEnded())
		{
			alg.Clean();
		}
	}
}

void AStarScene::OnDraw(sf::RenderWindow& renderWindow)
{
	for (auto& shape : table)
	{
		renderWindow.draw(shape);
	}
}
