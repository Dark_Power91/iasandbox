#pragma once

#include "IScene.h"
#include <algorithm>
#include <sstream>
#include <vector>
#include "FontManager.h"
#include "FifteenAStar.h"


class FifteenPuzzleScene : public IScene
{
	FifteenAStar alg;
	std::vector<sf::Text> texts;
	bool ended = false;

public:

	FifteenPuzzleScene();

	virtual void OnIdle();
	virtual void OnDraw(sf::RenderWindow&);

	virtual ~FifteenPuzzleScene();
};